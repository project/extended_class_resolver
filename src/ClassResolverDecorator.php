<?php

namespace Drupal\extended_class_resolver;

use Drupal\Core\DependencyInjection\ClassResolver;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class ClassResolverDecorator
 *
 * @package Drupal\extended_class_resolver\Decorator
 */
class ClassResolverDecorator extends ClassResolver {

  /**
   * Original object.
   *
   * @var ClassResolverInterface
   */
  protected $inner;

  public function __construct(ClassResolverInterface $inner) {
    $this->inner = $inner;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstanceFromDefinition($definition) {
    if ($this->container->has($definition)) {
      $instance = $this->container->get($definition);
    }
    else {
      if (!class_exists($definition)) {
        throw new \InvalidArgumentException(sprintf('Class "%s" does not exist.', $definition));
      }

      // Get any additional arguments, and remove the $definition argument.
      $args = func_get_args();
      array_shift($args);

      if (is_subclass_of($definition, 'Drupal\Core\DependencyInjection\ContainerInjectionInterface')) {
        $instance = $definition::create($this->container, ...$args);
      }
      else {
        $instance = new $definition(...$args);
      }
    }

    if ($instance instanceof ContainerAwareInterface) {
      $instance->setContainer($this->container);
    }

    return $instance;
  }

}
