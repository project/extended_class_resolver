Extended Class Resolver
=======================
THIS MODULE DOES NOTHING ON ITS OWN, IT IS FOR DEVELOPERS.

Introduction
------------
This module replaces the standard class resolver to allow parameters to be
added when instantiating a class using the ClassResolver mechanism.

This is what happens with plugins, but I guess they don't use the standard
class resolver.

Requirements
------------
This module requires no modules outside of Drupal core.

Installation
------------
Install the Extended Class Resolver module as you would normally install a
contributed  Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.

Configuration
-------------
None. It just works.

How to use it
-------------
This is a drop-in replacement for the class resolver service and as such it
starts working as soon as the module is enabled. It is completely backwards
compatible so any existing use is unaffected.

So something like this:
```php
/**
 * Implements hook_entity_type_build().
 */
function workspaces_entity_type_build(array &$entity_types) {
  return \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(EntityTypeInfo::class)
    ->entityTypeBuild($entity_types);
}
```
Will continue to work.

But with the extended version, so will this:
```php
function example(int $arg1, array &$arg2) {
  return \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(ExampleClass::class, $arg1, $arg2);
}
```
Where the `arg1` and `arg2` will be supplied to the constructor, or the
`create()` if the class being resolved has dependency injection.

